import re
import json
import PyPDF2
import os
# IMPORTANTE: hay metodos con  segundas versiones del mismo metodo a fin de tratar con mas posibles formatos

def info_funciones_datos():
    texto = """
    Librerías que se importan mediante esta librería:
    - re: Regex
    - json: Para la creación de ficheros JSON
    - PyPDF2: Para la obtención del texto de archivos PDF
    - os:  operaciones de sistema para guardar los json resultantes en una carpeta determinada

    Funciones auxiliares para la captura de datos:
    Cada función cuenta con su respectivo help(funcion) para entender lo que realiza:
    
    - encontrar_codigos_postales(texto)
    - encontrar_cif(texto)
    - compruebaFechasInicioFin(fecha)
    - compruebaNumeroFactura(numero)
    - compruebaNombreComercializadora(nombre)
    - dni(texto)
    - convertir_fecha(fecha)
    - extraer_direccion_comercializadora(texto)
    - get_poblacionProvincia(texto)
    - convertir_fecha2(fecha)
    - capturaNumeroFactura(texto)
    - extraer_direccion_comercializadora2(texto)
    - revisaImporteFactura(importe)
    """
    print(texto)
#variables para convertir_fecha
# Diccionario para mapear los nombres de los meses a sus valores numéricos
meses = {
    "enero": "01",
    "febrero": "02",
    "marzo": "03",
    "abril": "04",
    "mayo": "05",
    "junio": "06",
    "julio": "07",
    "agosto": "08",
    "septiembre": "09",
    "octubre": "10",
    "noviembre": "11",
    "diciembre": "12"
}


def revisaImporteFactura(importe):
    """
    Verifica si un importe tiene el formato correcto 'dddd,dd'.

    Args:
    importe (str): El importe en formato 'dddd,dd', donde 'dddd' representa de 1 a 4 dígitos antes de la coma y 'dd' representa exactamente 2 dígitos después de la coma.

    Returns:
    bool: True si el importe coincide con el formato, False en caso contrario.
    """
    # Expresión regular para validar el importe
    patron = r'^\d{1,4},\d{2}$'
    
    # Verificar si el importe coincide con el patrón
    if re.match(patron, importe):
        return True
    else:
        return False

def extraer_direccion_comercializadora2(texto):
    """
    Extrae la dirección de un texto antes de `, ` seguido de 5 dígitos.

    Args:
    texto (str): El texto que contiene la dirección completa.

    Returns:
    str: La dirección extraída si coincide con el patrón, de lo contrario devuelve el texto original.
    """
    # Expresión regular para capturar la dirección antes de `, ` seguido de 5 dígitos
    patron = r'^(.*?),\s*\d{5}\s*-\s*'
    
    # Buscar la coincidencia en el texto
    coincidencia = re.search(patron, texto)
    
    if coincidencia:
        # Extraer y devolver la parte coincidente
        return coincidencia.group(1).strip()
    else:
        # Devolver el texto original si no se encuentra el patrón
        return texto




def encontrar_codigos_postales(texto):
    """
    Encuentra todos los códigos postales de 5 dígitos en un texto.

    Args:
    texto (str): El texto que contiene los posibles códigos postales.

    Returns:
    set: Un conjunto de códigos postales únicos encontrados en el texto.
    """
  
    regex =  r'\b\d{5}\b'
    resultado = re.findall(regex, texto)
    r = set()
    for cp in resultado:
        r.add(cp)
    return r


    return resultado





def encontrar_cif(texto):
    """
    Encuentra todos los CIFs (Código de Identificación Fiscal) en un texto.

    Args:
    texto (str): El texto que contiene los posibles CIFs.

    Returns:
    set: Un conjunto de CIFs únicos encontrados en el texto.
    """
  
    regex = r'(?<!\w)[A-HJ-NP-SUVW]\d{8}(?!\w)'
    resultado = re.findall(regex, texto)
    return set(resultado)


def compruebaFechasInicioFin(fecha):
    """
    Comprueba y extrae una fecha en formato 'DD/MM/AAAA' desde un texto.

    Args:
    fecha (str): El texto que contiene la fecha.

    Returns:
    str: La primera fecha encontrada en el formato 'DD/MM/AAAA'. Retorna None si no se encuentra ninguna fecha.
    """
    # Expresión regular para buscar una fecha en formato DD/MM/AAAA
    patron = r'\b(\d{2}/\d{2}/\d{4})\b'
    
    # Buscar la primera coincidencia en el texto
    coincidencia = re.search(patron, fecha)
    
    if coincidencia:
        # Si se encuentra una coincidencia, retornar la fecha encontrada
        return coincidencia.group(1)
    else:
        # Si no se encuentra ninguna coincidencia, retornar None
        return None
    

def compruebaNumeroFactura(numero):
    """
    Verifica si un número de factura cumple con uno de los dos formatos válidos.

    Args:
    numero (str): El número de factura a verificar, que debe ser de la forma 'A1234567890' o 'AA1234567890'.

    Returns:
    bool: True si el número de factura coincide con alguno de los formatos válidos, False en caso contrario.
    """
# Expresión regular para verificar los dos formatos
    patron = r'^[A-Z]\d{10}$|^[A-Z]{2}\d{10}$'

    # Comprobar si el número coincide con el patrón
    if re.match(patron, numero):
        return True
    else:
        return False

def compruebaNombreComercializadora(nombre):
    """
    Verifica y devuelve todas las palabras en mayúsculas de un texto.

    Args:
    nombre (str): El texto que contiene posibles palabras en mayúsculas.

    Returns:
    str: Una cadena que contiene todas las palabras en mayúsculas encontradas en el texto, separadas por espacios.
    """
    # Expresión regular para encontrar todas las palabras en mayúsculas
    palabras_mayusculas = re.findall(r'\b[A-Z]+\b', nombre)
    # Unir las palabras encontradas en una sola cadena, separadas por espacios
    return ' '.join(palabras_mayusculas)



def dni(texto):
    """
    Busca un DNI (Documento Nacional de Identidad) en un texto.

    Args:
    texto (str): El texto que contiene el posible DNI.

    Returns:
    str: El DNI encontrado en el texto, incluyendo los 8 dígitos y la letra de control.
    """
     
    patron = r"(?:^|\s)(\d{8}[A-Z])(?:$|\s)"
    resultado = re.search(patron, texto)
        
    if resultado:
        return  resultado.group()
    



def convertir_fecha(fecha):
        
    """
    Convierte una fecha en formato 'día de mes de año' a 'dd/mm/yyyy'.

    Args:
    fecha (str): La fecha en formato 'día de mes de año'.

    Returns:
    str: La fecha en formato 'dd/mm/yyyy' si el formato es válido, de lo contrario None.
    """
 
    
    # Expresión regular para capturar el formato de la fecha
    patron = r'(\d{1,2}) de (\w+) de (\d{4})'
    coincidencia = re.match(patron, fecha.lower())
    
    if coincidencia:
        dia = coincidencia.group(1)
        mes_texto = coincidencia.group(2)
        anio = coincidencia.group(3)
        
        # Obtener el valor numérico del mes del diccionario
        mes_numero = meses.get(mes_texto)
        
        # Formatear la fecha en el nuevo formato
        fecha_formateada = f"{dia.zfill(2)}/{mes_numero}/{anio}"
        
        return fecha_formateada
    else:
        return None


def extraer_direccion_comercializadora(texto):
    """
    Extrae la dirección de un texto hasta justo antes de ', - ' seguido por cualquier nombre de provincia.

    Args:
    texto (str): El texto que contiene la dirección completa.

    Returns:
    str: La dirección extraída si coincide con el patrón, de lo contrario devuelve el texto original.
    """
    # Expresión regular para capturar la dirección hasta justo antes de `, - ` seguido por cualquier nombre de provincia
    patron = r'^(.*?),\s*-\s*(?:[\w\s\d.,/-]+)$'
    
    # Buscar la coincidencia en el texto
    coincidencia = re.search(patron, texto)
    
    if coincidencia:
        # Extraer y devolver la parte coincidente
        return coincidencia.group(1).strip()
    else:
        # Devolver el texto original si no se encuentra el patrón
        return texto




def get_poblacionProvincia(texto):
    """
    Obtiene la población y provincia desde un texto en formato 'código postal - población. provincia'.

    Args:
    texto (str): El texto que contiene la información de población y provincia.

    Returns:
    tuple: Una tupla que contiene la población y la provincia si se encuentra el patrón, de lo contrario devuelve (None, None).
    """
    # Expresión regular para buscar población y provincia
    pattern = r"(\d+)\s*-\s*([A-ZÁÉÍÓÚÜÑa-záéíóúüñ\s]+)\.\s*([A-ZÁÉÍÓÚÜÑa-záéíóúüñ\s]+)"
    
    # Buscar coincidencias en el texto
    match = re.search(pattern, texto)
    if match:
        poblacion = match.group(2).strip()
        provincia = match.group(3).strip()
        return poblacion, provincia
    else:
        return None, None
    

def convertir_fecha2(fecha):
    """
    
    Convierte unas fechas en formato 'día de mes de año' a 'dd/mm/yyyy'.

    Args:
    fecha (str): La fechas en formato 'día de mes de año'.IMPORTANTE: la diferencia con convertir_fecha es que este recibe las dos en un mismo string

    Returns:
    list: Una lista de fechas en formato 'dd/mm/yyyy' si se encuentran coincidencias, de lo contrario devuelve una lista vacía.
    """
    # Expresión regular para capturar el formato de la fecha
    patron = r'(\d{1,2}) de (\w+) de (\d{4})'
    coincidencias = re.findall(patron, fecha.lower())
    
    fechas_convertidas = []
    for coincidencia in coincidencias:
        dia = coincidencia[0]
        mes_texto = coincidencia[1]
        anio = coincidencia[2]
        
        # Obtener el valor numérico del mes del diccionario
        mes_numero = meses.get(mes_texto)
        
        # Formatear la fecha en el nuevo formato y agregarla a la lista
        fecha_formateada = f"{dia.zfill(2)}/{mes_numero}/{anio}"
        fechas_convertidas.append(fecha_formateada)
        
    return fechas_convertidas


def capturaNumeroFactura(texto):
    """
    Captura el número de factura en uno de dos formatos válidos.

    Args:
    texto (str): El texto que contiene el número de factura.

    Returns:
    str: El número de factura encontrado si coincide con alguno de los formatos válidos, None si no se encuentra ningún patrón.
    """
    # Expresión regular para verificar los dos formatos
    patron = r'\b[A-Z]{1,2}\d{10}\b'

    # Buscar la coincidencia en el texto
    coincidencia = re.search(patron, texto)
    
    if coincidencia:
        # Devolver el número de factura encontrado
        return coincidencia.group(0)
    else:
        # Devolver None si no se encuentra el patrón
        return None