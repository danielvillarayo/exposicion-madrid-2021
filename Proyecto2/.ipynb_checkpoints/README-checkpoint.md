## **Proyecto de Clustering, Recomendación y Predicción de Ratings para Restaurantes** ##
Este proyecto se compone de tres partes principales:

#### **Clustering de Restaurantes en Madrid** ####
Hemos aplicado técnicas de clustering utilizando el algoritmo **DBSCAN** y **KMeans** para agrupar restaurantes en Madrid. Los distintos clusters se forman en base a diversas características, principalmente el tipo de cocina, y se diferencian principalmente por el rango de precios. Esta parte del proyecto ofrece una visión estructurada de la diversidad de restaurantes en la ciudad y a quién van dirigidos.

#### **Recomendadores de Restaurantes** ####
Hemos desarrollado dos recomendadores de restaurantes, uno centrado en Madrid y otro en México, mediante una aplicación **web interactiva** creada con la biblioteca **Streamlit**. Estos recomendadores se basan en las puntuaciones que los usuarios otorgan a una serie de restaurantes. Utilizando esta información, el sistema recomienda otros restaurantes similares en función de sus características. Esta funcionalidad proporciona a los usuarios sugerencias personalizadas basadas en sus preferencias gastronómicas.

#### **Modelo Predictivo de Ratings** ####
Implementamos un modelo predictivo para estimar las calificaciones de los restaurantes según las revisiones proporcionadas por los usuarios. Logramos una tasa de predicción de más del 50%, lo que proporciona información valiosa sobre cómo los usuarios evalúan diferentes restaurantes.

Este proyecto refleja nuestro interés en explorar y aplicar técnicas de aprendizaje no supervisado, recomendación y predicción en el contexto de la industria de restaurantes, brindando perspectivas útiles y soluciones prácticas. **¡Explora el código y disfruta de las funcionalidades ofrecidas por cada componente!**

#### **Enlaces de interés:** ####
- Recomendador de restaurantes de México -> https://turecomendadorfavorito.streamlit.app/
- Recomendador de restaurantes de Madrid -> https://restaurantsrecomendator.streamlit.app/

#### Equipo: ####
<div style="display: flex; align-items: center;">
  <div>
    <h2>Ana Zubieta</h2>
    <p>:email: ena.ateibuz@gmail.com <br>
        &#x1F4F1; 681958123 <br>
        &#x1F468;&#x200D;&#x1F4BB; [www.linkedin.com/in/ana-zubieta](https://www.linkedin.com/in/ana-zubieta/) <br>
        &#x1F431; [GitHub](https://github.com/Ateibuzena)
    </p>
  </div>
  <img src="../Data/imagen_ane.jpg" alt="Descripción de la imagen" style="width: 100px; height: 100px; margin-left: 20px;">
</div>



<div style="display: flex; align-items: center;">
  <div>
    <h2>Lorena Martínez García</h2>
    <p>:email: lorena.aljorra1994@gmail.com <br>
        &#x1F4F1; 620031491 <br>
        &#x1F468;&#x200D;&#x1F4BB; <a href="https://www.linkedin.com/in/lorenamtnez/">www.linkedin.com/in/lorenamtnez</a> <br>
        &#x1F431; <a href="https://github.com/LorenaMtnez94">https://github.com/LorenaMtnez94</a>
    </p>
  </div>
  <img src="../Data/imagen_lorena.jpg" alt="Descripción de la imagen" style="width: 1.38in; height: 1.77in; margin-left: 20px;">
</div>



<div style="display: flex; align-items: center;">
  <div>
    <h2>María Gómez Román</h2>
    <p>:email: mariagomezr96@gmail.com <br>
        &#x1F4F1; 630867766 <br>
        &#x1F468;&#x200D;&#x1F4BB; [linkedin.com/in/mariagomezroman/](https://www.linkedin.com/in/mariagomezroman/) <br>
        &#x1F431; [https://github.com/mariagomez96-stack](https://github.com/mariagomez96-stack)
    </p>
  </div>
  <img src="../Data/imagen_maria.png" alt="Descripción de la imagen" style="width: 100px; height: 100px; margin-left: 20px;">
</div>


