import pandas as pd
import os



def excelToCsv(file_pathIn,file_path_out):

    # Ruta del archivo Excel
    file_path = file_pathIn

    # Crear la carpeta extract_data si no existe
    extract_folder = file_path_out
    os.makedirs(extract_folder, exist_ok=True)

    # Obtener los nombres de todas las hojas del archivo Excel
    sheet_names = pd.ExcelFile(file_path).sheet_names

    # Cargar cada hoja en un DataFrame y guardarla como un archivo separado
    for sheet_name in sheet_names:
        df = pd.read_excel(file_path, sheet_name=sheet_name)
        #IF para cambiar el nombre de la Main table ya que es bastante largo
        if 'MAIN' in sheet_name:
            df.to_csv(os.path.join(extract_folder, f'Main.csv'), index=False)
            print(f'{sheet_name} guardado correctamente. Con el nombre Main')
        else:
            df = pd.read_excel(file_path, sheet_name=sheet_name)
            df.to_csv(os.path.join(extract_folder, f'{sheet_name}.csv'), index=False)
            print(f'{sheet_name} guardado correctamente.')



def obtener_tablas_dict(folder_pathIn)-> dict:
    # Ruta de la carpeta con los CSV
    folder_path = folder_pathIn

    # Cargar cada CSV en un DataFrame y almacenarlo en un diccionario
    dataframes = {}
    for file_name in os.listdir(folder_path):
        if file_name.endswith('.csv'):
            sheet_name = file_name.replace('.csv', '')
            file_path = os.path.join(folder_path, file_name)
            
            try:
                df = pd.read_csv(file_path)
                if df.empty:
                    print(f'{sheet_name} está vacío y ha sido ignorado.')
                    continue
                dataframes[sheet_name] = df
                print(f'{sheet_name} cargado correctamente.')
            except pd.errors.EmptyDataError:
                print(f'{sheet_name} está vacío y ha sido ignorado.')
            except Exception as e:
                print(f'Error al cargar {sheet_name}: {e}')
    return dataframes



def informe_tablas(dic_tablas):
    dataframes = dic_tablas
    for sheet_name, df in dataframes.items():
        print("------------------------------------------------------------------------------------------")
        print("------------------------------------------------------------------------------------------")
        print("------------------------------------------------------------------------------------------")
        print(f"Explorando {sheet_name}:")
        print("-------------------------------------INFO-----------------------------------------------------")
        print(df.info())
        print("-------------------------------------DESCRIBE-----------------------------------------------------")
        print(df.describe())
        
        print("------------------------------------------------------------------------------------------")
        print("------------------------------------------------------------------------------------------")
        print("------------------------------------------------------------------------------------------")

